# library(RNASeqPower)
# library(dplyr)
# 
# #out <- readRDS("edgeR_DEGs_2019-12-10.rds")
# 
# # seqLength <- 50
# # aveNumReads <- mean(out$dgeList$samples$lib.size)
# # RatTranscriptomeLength <- readRDS("rn6_anno.rds") %>% .$width %>% sum
# # MouseTranscriptomeLength <- readRDS("mm10_anno.rds") %>% .$width %>% sum
# # aveTranscriptomeLength <- mean(RatTranscriptomeLength, MouseTranscriptomeLength) %>% round
# # aveDepth <- (aveNumReads*seqLength)/aveTranscriptomeLength
# 
# # Note: throughout this article, we refer to depth and coverage interchangeably — both 
# # meaning how many reads are assigned to a particular gene
# aveDepth <- out$rawCounts[,-(1:4)] %>% rowMeans() %>% median() %>% round
# sampleSize <- length(levels(out$experimentDetails$experimentVariables$rep))
# CV <- sqrt(out$dgeList$common.dispersion) %>% round(., 2)
# cvRange <- c(0.10, CV, 0.50, 1.00)
# effectRange <- seq(1, 25, by = 0.25)
# powerRanger <- seq(0.1, 0.9, by = 0.1)
#     
# powerCurve <- rnapower(n = sampleSize, depth = aveDepth, cv = cvRange, effect = effectRange, alpha = 0.05)
# powerCurveDat <- 
#     colnames(powerCurve) %>% as.numeric %>% 
#     rep(., each = nrow(powerCurve)) %>% as.data.frame() %>% 
#     set_colnames(., "Fold.Change")
# powerCurveDat$CV <- rownames(powerCurve) %>% as.numeric() %>% rep(., ncol(powerCurve))
# powerCurveDat$Power <- powerCurve %>% as.vector()
# 
# studyEmpPower <- rnapower(n = sampleSize, depth = aveDepth, cv = CV, power = 0.8, alpha = 0.05) %>% log2
# 
# pdf(file = "powerCurves.pdf")
# plot(log2(filter(powerCurveDat, CV == 1)$Fold.Change), 
#      filter(powerCurveDat, CV == 1)$Power, 
#      ylim = c(0,1), type = "b",
#      xlab = "Log Fold Change", ylab = "Power", 
#      pch = 20, cex = 1.2,
#      main = "Power Calculation for N = 4")
# points(log2(filter(powerCurveDat, CV == 0.5)$Fold.Change), 
#        filter(powerCurveDat, CV == 0.5)$Power, type = "b",
#        pch = 20, cex = 1.2, col = "orange")
# points(log2(filter(powerCurveDat, CV == 0.1)$Fold.Change), 
#        filter(powerCurveDat, CV == 0.1)$Power, type = "b",
#        pch = 20, cex = 1.2, col = "dodgerblue")
# points(log2(filter(powerCurveDat, CV == 0.25)$Fold.Change), 
#        filter(powerCurveDat, CV == 0.25)$Power, type = "b",
#        pch = 20, cex = 1.2, col = "red")
# 
# abline(h = 0.9, col = "black", lty = 2, lwd = 2)
# text(x = 3.0, y = 0.93, "Power = 0.9", cex = 1.2)
# 
# legend("bottomright", 
#        legend = c("CV = 0.1", "CV = 0.5", "CV = 1", "Average CV"), 
#        col = c("dodgerblue", "orange", "black", "red"), pch = 20)
# 
# dev.off()
# 
# 
# pdf(file = "powerCurvesAtEmpiricalCV.pdf")
# plot(log2(filter(powerCurveDat, CV == 0.25)$Fold.Change), 
#      filter(powerCurveDat, CV == 0.25)$Power, 
#      ylim = c(0,1), type = "b",
#      xlab = "Log2 Fold Change", ylab = "Power", 
#      pch = 20, cex = 1.2,
#      main = "Power Calculation for N = 4")
# abline(h = 0.9, col = "red", lty = 2, lwd = 2)
# text(x = 4.0, y = 0.93, "Power = 0.9", cex = 1.2)
# #lines(x = rep(studyEmpPower,2), y = c(0,0.9))
# dev.off()
# 
# 
# pdf(file = "powerCurvesAtEmpiricalCVwithLineLowerPower.pdf", width = 9, height = 7)
# plot(log2(filter(powerCurveDat, CV == 0.25)$Fold.Change), 
#      filter(powerCurveDat, CV == 0.25)$Power, 
#      ylim = c(0,1), type = "b",
#      xlab = "Log2 Fold Change", ylab = "Power", 
#      pch = 20, cex = 1.2,
#      main = "Power Calculation for N = 4")
# abline(h = 0.8, col = "red", lty = 2, lwd = 2)
# text(x = 0.25, y = 0.93, "Power = 0.9", cex = 1.2)
# lines(x = rep(studyEmpPower,2), y = c(-0.1,0.8), lwd = 2, lty = 2, col = "red")
# text(x = 1.5, y = 0, paste("Log2FC =", round(studyEmpPower, 2)), cex = 1.2)
# dev.off()
