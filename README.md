
# HSSRscripts

The goal of HSSRscripts is to aggregate functions that we use regularly at HSS
and place them in convenient location for maintaining and testing.

## Installation

You can install HSSRscripts from [Gitlab](https://gitlab.com/hssgenomics/hssrscripts) with:

``` r

devtools::install_gitlab("hssgenomics/hssrscripts")

```

## Examples

``` r

library(HSSRscripts)

## basic example code


```

